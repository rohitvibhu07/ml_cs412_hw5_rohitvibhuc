This is a Machine Learning Project. The young People survey dataset is taken and tried to predict how empthetic can a person be.

Dowload the dataset from : 
https://www.kaggle.com/miroslavsabo/young-people-survey/

To Run this, have the necessary softwares: 

1. Python 3, preferably 3.6
2. pandas
3. scikit learn
4. numpy.
5. seaborn
6. Matplotlib

if not please pip install all the latest versions of the above said libraries.

Once, everything is set up. Open the homeword_5.py file in a IDE like pycharm,spyder. 
In the main function, change the path to the location containing responses.csv file.

Than run the entire file, to see the baseline prediction accuracy and Best model Classifier Accuracy.