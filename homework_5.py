import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.svm import LinearSVC, SVC
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.ensemble import AdaBoostClassifier

#get_ipython().run_line_magic('matplotlib', 'inline')


# With the use of pandas, we make a data frame of the entire csv file to handle better.
def read_input(fielname):
    print("Reading the file into a  dataframe")
    df = pd.read_csv('responses.csv')
    return df


def check_heatmap_null(df):
    print("To check the nan values in the entire dataset.")
    sns.heatmap(df.isnull(), yticklabels=False, cmap='viridis')


def drop_null_rows_of_Label(df_copy, label):
    print("Dropping Rows for NULL Values in Empathy Column")
    df_copy.dropna(subset=[label], inplace=True)
    df_copy.reset_index(inplace=True, drop=True)
    return df_copy


def replace_categorical(value, mode):
    if pd.isnull(value):
        value = mode
    return value


def handle_null_values(df_copy):
    print("Numerical Columns are filled with mean of Column")
    null_list = list(df_copy.columns[df_copy.isnull().any()])
    for i in null_list:
        if df_copy[i].dtype != 'O':
            df_copy[i].fillna(value=round(df_copy[i].mean()) % 5, inplace=True)

    # sns.heatmap(df_copy.isnull(),yticklabels=False,cmap = 'viridis')
    null_categical = list()
    for i in list(df_copy.columns):
        if df_copy[i].dtype == 'O':
            null_categical.append(i)

    print("Categorical Columns are filled with mode of Column")
    for i in null_categical:
        df_copy[i] = df_copy[i].apply(lambda value: replace_categorical(value, list(df_copy[i].mode())[0]))

    return df_copy, null_categical


def Label_encode(data, column_name):
    values = np.array(data)
    le = LabelEncoder()
    result = le.fit_transform(values)
    return result


def convert_categorical_to_numerical(df_copy, null_categical):
    df_create = pd.DataFrame()
    ##Call the Above Function
    print("Convert the Categorical values to Numerical values ")
    for i in null_categical:
        result = Label_encode(df_copy[i], i)
        df_result = pd.DataFrame(result, columns=[i])
        df_create = pd.concat([df_create, df_result], axis=1)

    # Drop the categorical columns from initial table
    for i in null_categical:
        df_copy.drop([i], inplace=True, axis=1)
        df_copy.reset_index(inplace=True, drop=True)

    # Concat the created label encoded columns back to initial Dataframe
    df_copy = pd.concat([df_copy, df_create], axis=1)

    return df_copy


def make_feature_label(df_copy, label):
    X = df_copy.drop([label], axis=1)
    y = df_copy[label]
    return X, y


# Select K best method for feature Selection.
def Kbest_features(X, y):
    print("Select K best features")
    X_new = SelectKBest(chi2, k=15).fit_transform(X, y)
    return X_new


# Select Features on Tree Based Selection.
def tree_based_feature_selection(X, y):
    print("Select features based on Tree feature importances")
    tree_select = RandomForestClassifier(n_estimators=100, max_depth=50, max_features=100)
    tree_select.fit(X, y)
    # print(clf.feature_importances_)
    model = SelectFromModel(tree_select, prefit=True)
    X_new = model.transform(X)
    print(X_new.shape)
    return X_new


def train_test_dev_split(X_new, y):
    print("splitting data in to Train test and development set")
    X_train, X_test, y_train, y_test = train_test_split(X_new, y, test_size=0.2, random_state=42)
    X_train, X_dev, y_train, y_dev = train_test_split(X_train, y_train, test_size=0.1, random_state=42)
    return X_train, X_test, X_dev, y_test, y_train, y_dev

#Baseline Model
def decision_tree_model(X_train, X_test, X_dev, y_test, y_train, y_dev):
    print('Baseline Model - Decision Tree')
    dtree = DecisionTreeClassifier(max_depth=25)
    dtree.fit(X_train, y_train)
    pred = dtree.predict(X_dev)
    print('Development Accuracy')
    print(np.mean(y_dev == pred))
    pred_test_dtree = dtree.predict(X_test)
    print('Test Accuracy:')
    print(np.mean(pred_test_dtree == y_test))
    print("\t\t\t\t--------------------------------\t\t\t\t")

def random_forest_model(X_train, X_test, X_dev, y_test, y_train, y_dev):
    print(" *****Random Forest Model*****")
    rd = RandomForestClassifier(n_estimators=500, max_depth=10, max_features=10)
    rd.fit(X_train, y_train)
    pred_rd = rd.predict(X_dev)
    print('Development Accuracy')
    print(np.mean(y_dev == pred_rd))
    pred_random_test = rd.predict(X_test)
    print('Test Accuracy')
    print(np.mean(pred_random_test == y_test))

# # Linear SVC
def svc_model(X_train, X_test, X_dev, y_test, y_train, y_dev):
    linear_svc = SVC()
    linear_svc.fit(X_train, y_train)
    pred_svc = linear_svc.predict(X_dev)
    print(np.mean(pred_svc == y_dev))
    pred_svc_test = linear_svc.predict(X_test)
    print(np.mean(pred_svc_test == y_test))


# # AdaBoosting
def rf_ada_model(X_train, X_test, X_dev, y_test, y_train, y_dev):
    print(" *****Random Forest Model With Adaboosting*****")
    rd_Ada = RandomForestClassifier(n_estimators=1000, max_depth=5, max_features=10)
    adaboost = AdaBoostClassifier(base_estimator=rd_Ada)
    adaboost.fit(X_train, y_train)
    pred_ada_dev = adaboost.predict(X_dev)
    print(np.mean(pred_ada_dev == y_dev))
    pred_ada_test = adaboost.predict(X_test)
    print(np.mean(pred_ada_test == y_test))


if __name__ == '__main__':
    #Enter the path to the csv file. By default it searches for the file in the same folder.
    df_copy = read_input(u'.\\responses.csv')
    df_copy = drop_null_rows_of_Label(df_copy, 'Empathy')
    df_copy, null_categical = handle_null_values(df_copy)
    df_copy = convert_categorical_to_numerical(df_copy , null_categical)
    X, y = make_feature_label(df_copy, 'Empathy')
    X_train, X_test, X_dev, y_test, y_train, y_dev = train_test_dev_split(X, y)
    # Baseline model
    decision_tree_model(X_train, X_test, X_dev, y_test, y_train, y_dev)
    #select_input = input("1 - Kbest Features, 2 - Selectfrommodel")
    #     if select_input == '1':
    #         X_new = Kbest_features(X,y)
    #     else:
    #         X_new = tree_based_feature_selection(X,y)
    X_new = tree_based_feature_selection(X, y)
    X_train, X_test, X_dev, y_test, y_train, y_dev = train_test_dev_split(X_new, y)
    #Random Forest Model
    random_forest_model(X_train, X_test, X_dev, y_test, y_train, y_dev)